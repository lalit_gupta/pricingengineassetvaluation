from flask import Flask
import pandas as pd
import datetime
import datetime as dt
from flask import jsonify,request
from flask_cors import CORS
from base64 import b64decode
import json
import math

app=Flask(__name__)
CORS(app)

ivr_file=pd.read_csv('ivr_calculation_file/IVR_Output_upd.csv')

response_to_weight_mapping={
    "Poor":-1,
    "Average":0,
    "Good":0.5,
    "Excellent":1
}

#reading the csv files globally
asset_caterogy_to_file_mapping={
    "backhoe loader group":pd.read_csv('valuation_lookup_files/bhl.csv'),
    "excavator group":pd.read_csv('valuation_lookup_files/excavator.csv'),
    "tipper group":pd.read_csv('valuation_lookup_files/tipper.csv'),
    "tractor group":pd.read_csv('valuation_lookup_files/tractor.csv')
}


framed_question_lookup ={
    "backhoe loader group":"Backhoe Loader",
    "excavator group":"Excavator",
    "tipper group":"Tipper",
    "tractor group":"Tractor"
}

EXTENT_OF_NOTCH=0.2

def fetch_invoice_value(months,filtered_df):
    years=int(round(months/12.0,0))
    if years==0:
        years = 1

    print("years {}\nmonths {}".format(years,months))
    invoice_val=filtered_df['{}_year'.format(years)].iloc[0]
    print("invoice value {}\n\n\n".format(invoice_val))
    print("type {}".format(type(invoice_val)))
    return invoice_val

@app.route('/health')
def healthCheck():
    return "Ok"

@app.route("/calculateScore",methods=['POST'])
def pricingScoreCalculation():
    data=json.loads(request.data)
    print(type(data))

    invoice_date=data['invoice_date'] #%Y-%m-%d format
    invoice_date=map(int,invoice_date.split("-"))
    today=map(int,datetime.datetime.today().strftime("%Y-%m-%d").split("-"))

    days = abs((datetime.date(invoice_date[0], invoice_date[1], invoice_date[2])
                -
                datetime.date(today[0], today[1],today[2])).days)

    months=max(0,int(round(days/30,0)))

    quarters=int(round(months/3,0))

    try:
        assetFilter = ivr_file[(ivr_file['Asset_Category'] == data['asset_category'].upper())
                       &
                       (ivr_file['Manufacturer_Name'] == data['asset_manufacturer'].upper())
                       &
                       (ivr_file['Asset_Description'] == data['asset_model'].upper())]

        ivr=assetFilter[assetFilter['Quarter']=="Q{}".format(int(quarters))]['IVR'].values[0]
    except:
        return jsonify({"error":"Invalid Asset Details"})

    try:
        invoice_value= float(data['invoice_value'])
        if invoice_value>0:
            base_rate=ivr*invoice_value
        else:
            invoice_value=fetch_invoice_value(months,assetFilter)
            print("invoice value was <=0 , new fetched invoice values {}".format(invoice_value))
            base_rate=ivr*invoice_value
    except KeyError as e:
        print("Error {}".format(e))
        invoice_value = fetch_invoice_value(months, assetFilter)
        print("invoice value parameter was missing, new fetched invoice values {}".format(invoice_value))
        base_rate = ivr * invoice_value


    TA_Weightage=0
    SME_Weightage=0

    try:
        #print("Asset category ",data['asset_category'].lower())
        quesFile=asset_caterogy_to_file_mapping[data['asset_category'].lower()]
        quesFile['question_name'] = quesFile['question_name'].map(lambda x: str(x).strip().lower())
        quesFile['category_name'] = quesFile['category_name'].map(lambda x :str(x).strip().lower())

    except Exception as e:
        print(e)
        return jsonify({"Error":"Invalid Asset Category"})

    try:
        for questions in data['questions']:
            question_name= b64decode(questions['question_name'])
            category_name= b64decode(questions['category_name'])
            #print("question: {}\ncategory: {}\n".format(question_name, category_name))
            questionData = quesFile[(quesFile['question_name']==question_name.lower())
                                         &(quesFile['category_name']==category_name.lower())]
            responseWeight = response_to_weight_mapping[questions['response'].capitalize()]
            TA_Weightage+= responseWeight*questionData['Final_Weights_TA'].values[0]
            SME_Weightage+= responseWeight*questionData['Weights_SME'].values[0]
    except Exception as e:
        return jsonify({"message":"Error","reason":e})

    print("TA_Weights Sum--> {}".format(TA_Weightage))
    print("SME_Weights sum--> {}".format(SME_Weightage))

    basePriceNotched=base_rate*EXTENT_OF_NOTCH
    framed_questions_file=pd.read_csv("framed_questions.csv")
    #print(framed_question_lookup[data['asset_category']])
    framed_question_to_return=list(framed_questions_file[framed_questions_file['Asset Category']==
                                                         "{}".format(framed_question_lookup[data['asset_category'].lower()])]
                                                            ['Question Framed'])

    framed_question={}
    for fq in range(len(framed_question_to_return)):
        framed_question['question_{}'.format(fq+1)]=framed_question_to_return[fq]


    Final_Price_TA=(TA_Weightage*basePriceNotched)+base_rate
    Final_Price_SME=(SME_Weightage*basePriceNotched)+base_rate

    print("Final_Price_TA {}".format(Final_Price_TA))
    print("Final_Price_SME {}".format(Final_Price_SME))

    return jsonify({
                    "final_price_ta":round(Final_Price_TA,2),
                    "final_price_sme":round(Final_Price_SME,2),
                    "framed_questions":framed_question})

if __name__=="__main__":
    app.run(host='0.0.0.0',port=4444,debug=True)